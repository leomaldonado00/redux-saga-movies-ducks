import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Provider } from "react-redux";
import PropTypes from "prop-types";
import Templates from './templates';


const App = ({ store }) => (
  
 
      <div>
       <Templates store={store} />
      </div>
  

);

/* App.propTypes = {
  store: PropTypes.object.isRequired
}; */

export default App;
