import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import configureStore from './ducks';
import Templates from './templates';
import rootSaga from './ducks/sagas';
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";

import App from './App';


const {store, persistor, sagaMiddleware} = configureStore();
sagaMiddleware.run(rootSaga);

ReactDOM.render(
    <Provider store={store} >
        <BrowserRouter>
        <PersistGate persistor={persistor}>
        <Templates />
        </PersistGate>
            
        </BrowserRouter>
    </Provider>    , document.getElementById('root'));

