import React from "react";
import { withRouter } from "react-router-dom";

const MovieResult = ({ Title, Year, Type, imdbID, Poster, history }) => {
  console.log("IMDBID ", imdbID);
  const handleSeeMovieClick = () => {
    history.push(`/movie/${imdbID}`);
  };
  console.log(history);
  return (
    <div>
      <img src={Poster} alt={Title} />
      <h1>{Title}</h1>
      <h3>{Year}</h3>
      <h3>{Type}</h3>
      <button onClick={handleSeeMovieClick}>See more</button>
      <hr />
    </div>
  );
};

export default withRouter(MovieResult);