import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch
} from "react-router-dom";
import Home from "./home/Home";
import Results from "./home/Results";
import MovieDetail from "./home/MovieDetail";
import Phold from "./home/Phold";
import DetailPost from "./home/DetailPost";
import ProfilePost from "./home/ProfilePost";
import Comments from "./home/Comments";

export default () => {
  return (
    <div>
      <Router>
        <div>
          <Route exact path="/" component={Home} />
          <Route path="/results" component={Results} />
          <Route path="/movie/:id" component={MovieDetail} />
          <Route path="/pHold" component={Phold} />
          <Route path="/detailPost/:id" component={DetailPost} />
          <Route path="/profilePost/:userId" component={ProfilePost} />
          <Route path="/comments" component={Comments} />
        </div>
      </Router>
    </div>
  );
};
