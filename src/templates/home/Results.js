import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import queryString from "query-string";

import {
  searchMovie,
  movieResults,
  isSearchLoading,
  error
} from "../../ducks/access";
// import { searchMovie } from "../../redux/actions/search";
// import { movieResults, isSearchLoading } from "../../redux/selectors";
import MovieResult from "../MovieResult";
import Loading from "../Loading";

const Results = ({ location }) => {
  const dispatch = useDispatch();
  const movies = useSelector(state => movieResults(state)); // obtiene de los selectors el state especificamente "search.movieResults.Search"
  const isLoading = useSelector(state => isSearchLoading(state));
  const errorTemp = useSelector(state => error(state));
  const [isLooked, setIsLooked] = useState(false);

  useEffect(() => {
    console.log(location);
    const { movieName } = queryString.parse(location.search);

    if (movieName && !isLooked) {
      setIsLooked(true);
      dispatch(searchMovie({ movieName }));
    }
  }, [location.search, isLooked, dispatch, location]); // pasando como 2do argumento [location] evita bucle infinito

  const renderMovies = () => {
    // YOUTUBE, Se queda pegado en Loading cuando no encuentra resultados :s
    if (movies) {
      return movies.map((value, index) => (
        <MovieResult key={index} {...value} />
      ));
    } else if (isLoading) {
      return <Loading />;
    } else if (errorTemp) {
      return <div>{errorTemp}</div>;
    } else {
      return <div>No results</div>;
    }
  };
  console.log(movies);
  return (
    <div>
      {renderMovies()}

      {/* movies ? (
        movies.map((value, index) => <MovieResult key={index} {...value} />)
      ) : isLoading ? (
        <Loading />
      ) : (
        <div>No results</div>
      ) */}

      {/* movies !== undefined // RICARDO
        ? movies.map((value, index) => {
            return (
              <div key={index}>
                <img src={value.Poster} alt={value.Title} />
                <h1>{value.Title}</h1>
                <h3>{value.Year}</h3>
                <h3>{value.Type}</h3>
                <button>Ver más</button>
                <hr />
              </div>
            );
          })
        : null */}
    </div>
  );
};

export default Results;

// ALBERT
