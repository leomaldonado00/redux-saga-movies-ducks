import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import Loading from "../Loading";
import {
  isSearchLoading, // var
  detailPost, // var
  // comments,
  detailPostById,
  error // var
} from "../../ducks/rebusPhold";
// import { dispatch } from "../../../../../AppData/Local/Microsoft/TypeScript/3.6/node_modules/rxjs/internal/observable/pairs";

const DetailPost = ({ match, history }) => {
  const dispatch = useDispatch();
  const isLoading = useSelector(state => isSearchLoading(state));
  const detailPostTemp = useSelector(state => detailPost(state));
  const errorTemp = useSelector(state => error(state));
  // const commentsTemp = useSelector(state => comments(state));

  useEffect(() => {
    // console.log("useEffect_DetailPost");
    console.log("match_DetailPost", match);
    const idPostDetail = match.params.id;
    dispatch(detailPostById({ idPostDetail }));
  }, [dispatch, match]);

  const seeComments0 = id => {
    // dispatch(seeComments({ id }));
    history.push(`/comments?postId=${id}`);
  };

  return (
    <div>
      {isLoading ? <Loading /> : null}

      {detailPostTemp ? (
        <div>
          <button
            onClick={() =>
              history.push("/pHold", {
                ancla: ">>CONTENIDO DEL ESTADO DE HISTORY LOCATION<<"
              })
            }
          >
            {" "}
            Phold (enviando STATE de history.location){" "}
          </button>
          <button onClick={() => history.goBack()}>BACK </button>

          {
            <div>
              {" "}
              {detailPostTemp && detailPostTemp.userId} .{" "}
              {detailPostTemp && detailPostTemp.id} -{" "}
              {detailPostTemp && detailPostTemp.title} :{" "}
            </div>
          }
          {<div> {detailPostTemp && detailPostTemp.body} </div>}

          <button
            onClick={() => {
              seeComments0(detailPostTemp.id);
            }}
          >
            VER COMMENTS
          </button>
          {/* commentsTemp.map((comm, i) => (
                <div key={comm.id}>
                <h1>
                    {comm.postId} - {comm.name}
                </h1>
                <div>{comm.email}</div>
                <p>{comm.bady}</p>
                </div>
            )) */}
        </div>
      ) : null}

      {errorTemp ? <h1>{errorTemp}</h1> : null}
    </div>
  );
};

export default DetailPost;
