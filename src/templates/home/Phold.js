import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import Loading from "../Loading";
import {
  initialUsers,
  show10,
  postUser,
  upDateUser,
  upDatePart,
  deleteUser,
  users, // var
  isSearchLoading, // var
  scroll10, // var
  error // var
} from "../../ducks/rebusPhold";

const Phold = ({ history }) => {
  const dispatch = useDispatch();
  const usersList = useSelector(state => users(state)); // obtiene de los selectors el state especificamente "search.movieResults.Search"
  const isLoading = useSelector(state => isSearchLoading(state));
  const scroll10var = useSelector(state => scroll10(state));
  const errorTemp = useSelector(state => error(state));

  const [user, setUser] = useState({ userId: "", title: "", body: "" });
  const [id, setId] = useState("");

  useEffect(() => {
    // Actualiza el título del documento usando la API del navegador
    /*if(history.location.state && history.location.state.ancla){
            console.log("STATE de (history.location.state.ancla) HOME from detail", history.location.state.ancla); // RECIBIENDO el estado del history.location
          }*/
    console.log("useEffect_tmeplate");
    dispatch(initialUsers());
    //initialUsers();
    //setLoading(false);
  }, [dispatch]);

  useEffect(() => {
    if (scroll10var) {
      window.scroll(0, 10000000);
    }
  }, [scroll10var]);

  function setUserValid(e) {
    setUser({ ...user, [e.target.name]: e.target.value });
    if (e.target.value === "") {
      setUser({ ...user, [e.target.name]: "" });
    }
  }
  const postUser0 = user => {
    dispatch(postUser({ user }));
    setUser({ userId: "", title: "", body: "" }); // *************************************************
    setId("");
  };
  const show10_0 = event => {
    dispatch(show10());
  };
  const upDatePart0 = (id, user) => {
    console.log("upDatePart0", id, user);
    dispatch(upDatePart({ id, user }));
    setUser({ userId: "", title: "", body: "" }); // *************************************************
    setId("");
  };
  const upDateUser0 = (id, user) => {
    dispatch(upDateUser({ id, user }));
    setUser({ userId: "", title: "", body: "" }); // *************************************************
    setId("");
  };
  const deleteUser0 = id => {
    dispatch(deleteUser({ id }));
  };
  const User0 = id => {
    history.push(`/detailPost/${id}`);
  };
  const profile0 = userId => {
    history.push(`/profilePost/${userId}`);
  };

  console.log(errorTemp);

  return (
    <div>
      {isLoading ? <Loading /> : null}

      {usersList ? (
        <div>
          JSONplaceHolder
          <button onClick={() => history.push("/")}>MOVIES</button>
          <input
            onChange={e => setUserValid(e)}
            type="text"
            name="userId"
            placeholder="userId"
            value={user.userId}
          />
          <input
            onChange={e => setUserValid(e)}
            type="text"
            name="title"
            placeholder="title"
            value={user.title}
          />
          <input
            onChange={e => setUserValid(e)}
            type="text"
            name="body"
            placeholder="body"
            value={user.body}
          />
          {/*<input onChange={(e) => setUser({...user, [e.target.name]:e.target.value})} type="password" name="address"  placeholder="contraseña"  />*/}
          <button onClick={event => postUser0(user)}>ENVIAR </button>
          <input
            onChange={e => setId(e.target.value)}
            type="text"
            name="id"
            placeholder="id"
            value={id}
          />
          <button onClick={() => upDateUser0(id, user)}>
            ACTUALIZAR TODO (put){" "}
          </button>
          <button onClick={() => upDatePart0(id, user)}>
            ACTUALIZAR PARTE (patch){" "}
          </button>
          <h1>POSTS:</h1>
          {usersList
            ? usersList.map((user, i) => (
                <div key={user.id}>
                  <h1>
                    {user.userId} {user.title}
                  </h1>
                  <div>{user.body}</div>
                  <button onClick={() => deleteUser0(user.id)}>X</button>
                  <button
                    onClick={() => {
                      User0(user.id);
                    }}
                  >
                    Detail
                  </button>
                  <button
                    onClick={() => {
                      profile0(user.userId);
                    }}
                  >
                    Profile
                  </button>
                </div>
              ))
            : null}
          <button
            id="button_10"
            onClick={() => {
              show10_0();
            }}
          >
            {" "}
            + 10{" "}
          </button>
        </div>
      ) : null}

      {errorTemp ? <h1>{errorTemp}</h1> : null}
    </div>
  );
};

export default Phold;
