import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import Loading from "../Loading";
import {
  isSearchLoading, // var
  postsUserId,
  postsUserIdList, // var
  error // var
} from "../../ducks/rebusPhold";
// import { dispatch } from "../../../../../AppData/Local/Microsoft/TypeScript/3.6/node_modules/rxjs/internal/observable/pairs";

const ProfilePost = ({ match, history }) => {
  const dispatch = useDispatch();
  const isLoading = useSelector(state => isSearchLoading(state));
  const postsUserIdTemp = useSelector(state => postsUserIdList(state));
  const errorTemp = useSelector(state => error(state));

  useEffect(() => {
    // console.log("match.params.userId_profilePost", match.params.userId);
    const userId = match.params.userId;
    dispatch(postsUserId({ userId }));
  }, [dispatch, match.params.userId]);

  const User0 = id => {
    history.push(`/detailPost/${id}`);
  };

  return (
    <div>
      {isLoading ? <Loading /> : null}

      {postsUserIdTemp ? (
        <div>
          <h1>
            PROFILE UserId: {postsUserIdTemp ? postsUserIdTemp[0].userId : null}
          </h1>
          {postsUserIdTemp
            ? postsUserIdTemp.map((post, i) => (
                <div key={post.id}>
                  <h1>
                    {post.userId} {post.title}
                  </h1>
                  <div>{post.body}</div>
                  <div>address es un objeto</div>

                  <button
                    onClick={() => {
                      User0(post.id);
                    }}
                  >
                    Detail
                  </button>
                </div>
              ))
            : null}
        </div>
      ) : null}

      {errorTemp ? <h1>{errorTemp}</h1> : null}
    </div>
  );
};

export default ProfilePost;
