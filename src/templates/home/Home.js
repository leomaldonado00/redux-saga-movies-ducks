import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addText , saved, cutText} from '../../ducks/access';

// import { saved } from '../../../ducks/access';

const Home = ({ history }) => {
  const dispatch = useDispatch();
  const savedList = useSelector(state => saved(state)); // obtiene de los selectors el state especificamente "search.movieResults.Search"

  const [searchText, setSearchText] = useState("");
  const handleSearchTextChange = event => {
    setSearchText(event.target.value);
  };
  const handleCleanTextClick = event => {
    setSearchText("");
  };
  const handleSearchTextClick = event => {
    history.push(`results?movieName=${searchText}`);
  };
  const handleSesionPholdClick = event => { // CONFECCIONAR ESTA PAGINA PARA LA PARTE DE JSON PLACEHOLDER ****************
    history.push(`/pHold`);
  };
  const handleAddTextClick = event => {
    dispatch(addText({ searchText }));
  };
  const handleCutTextClick = (index,event) => {
     // console.log(index, event.target);
    dispatch(cutText({ index }));
  };
  return (
    <div>
      <p>Note: Enter the name of a movie to search results, then you can click on "See more" to go to the details. (example: Troy, Titanic, Iron Man). With the "add" button you can save your searches (only locally).</p>
      <hr/>

      <h1>Welcome to "Albert Movies"</h1>
      <input
        onChange={handleSearchTextChange}
        type="text"
        name="movie"
        placeholder="Search..."
        value={searchText}
      />
      <button onClick={handleCleanTextClick}>Clear</button>
      <button onClick={handleSearchTextClick}>Search</button>
      <button onClick={handleAddTextClick}>Add</button>
      <hr />

      <h5>Added:</h5>
      <ul>
        {savedList
          ? savedList.map((value, index) => (<li key={index} >{value} <button onClick={(event) => handleCutTextClick(index,event)}>x</button></li>))
          : null}
      </ul>
    </div>
  );
};

export default Home;