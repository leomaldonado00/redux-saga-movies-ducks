import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { movieResult as MovieResultSelector } from "../../ducks/access";
import { searchMovieById, isSearchLoading, error } from "../../ducks/access";
import Loading from "../Loading";

const MovieDetail = ({ match }) => {
  const dispatch = useDispatch();
  const movieResult = useSelector(state => MovieResultSelector(state)); // obtiene de los selectors el state especificamente "search.movieResult"
  const isLoading = useSelector(state => isSearchLoading(state));
  const errorTemp = useSelector(state => error(state));

  useEffect(() => {
    console.log("match_MovieDetail", match); // obj...isExact: true, params: {id: "tt0050672"}, path: "/movie/:id", url: "/movie/tt0050672"
    const movieId = match.params.id;
    // if (!movieResult || (movieResult && movieResult.imdbID !== movieId)) {
    // IF condicion para que no lo llame cada vez que actualiza
    dispatch(searchMovieById({ movieId })); // match: como parametro en components que son DESCENDIENTES de una RUTA !!
    // }
  }, [dispatch, match]); // pasando como 2do argumento [match] evita que se quede en una sola informacion de movie (si varia)

  /*if (!movieResult) {
    return <Loading />;
  }*/
  console.log(isLoading, movieResult, error);
  return (
    <div>
      {isLoading ? <Loading /> : null}

      {movieResult ? (
        <div>
          <h1>{movieResult.Title}</h1>
          <img src={movieResult.Poster} alt={movieResult.Title} />
          <p>
            {" "}
            <strong>Actores:</strong> {movieResult.Actors}{" "}
          </p>
          <p>
            {" "}
            <strong>Director:</strong> {movieResult.Director}{" "}
          </p>
          <p>
            {" "}
            <strong>Pais:</strong> {movieResult.Country}{" "}
          </p>
          <p>
            {" "}
            <strong>Clasificación:</strong> {movieResult.Rated}{" "}
          </p>
          <p>
            {" "}
            <strong>Premios:</strong> {movieResult.Awards}{" "}
          </p>
          <p>
            {" "}
            <strong>Psinopsis:</strong> {movieResult.Plot}{" "}
          </p>
        </div>
      ) : null}

      {errorTemp ? <h1>{errorTemp}</h1> : null}
    </div>
  );
};

export default MovieDetail;
