import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import queryString from "query-string";

import Loading from "../Loading";
import {
  isSearchLoading, // var
  comments, // var
  seeComments,
  error // var
} from "../../ducks/rebusPhold";
// import { dispatch } from "../../../../../AppData/Local/Microsoft/TypeScript/3.6/node_modules/rxjs/internal/observable/pairs";

const Comments = ({ location, history }) => {
  const dispatch = useDispatch();
  const isLoading = useSelector(state => isSearchLoading(state));
  const commentsTemp = useSelector(state => comments(state));
  const errorTemp = useSelector(state => error(state));

  useEffect(() => {
    const { postId } = queryString.parse(location.search);
    console.log("location.search_Comments", postId);
    dispatch(seeComments({ postId }));
  }, [dispatch, location.search]);

  console.log("commentsTemp", commentsTemp);

  return (
    <div>
      {isLoading ? <Loading /> : null}

      {commentsTemp ? (
        <div>
          <h1>
            COMMENTS del postId{" "}
            {commentsTemp ? commentsTemp[0].postId : "(Error)"}
          </h1>
          {commentsTemp
            ? commentsTemp.map((comm, i) => (
                <div key={comm.id}>
                  <h2>
                    {comm.postId} - {comm.name}
                  </h2>
                  <div>{comm.email}</div>
                  <p>{comm.body}</p>
                </div>
              ))
            : null}
        </div>
      ) : null}

      {errorTemp ? <h1>{errorTemp}</h1> : null}
    </div>
  );
};

export default Comments;
