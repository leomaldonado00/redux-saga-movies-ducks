import axios from "axios";

const BASE_URL = "https://www.omdbapi.com/?apiKey=ffd0c3a5";

const apiCall = (url, data, headers, method) =>
  axios({
    method,
    url: BASE_URL + url,
    data,
    headers
  });

const PLACEH_URL = "https://jsonplaceholder.typicode.com";

const apiPlaceHolder = (url, data, headers, method) =>
  axios({
    method,
    url: PLACEH_URL + url,
    data,
    headers
  });

export { apiCall, apiPlaceHolder };
