import { createStore, applyMiddleware, combineReducers, compose } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

import createSagaMiddleware from "redux-saga";
import accessStore from "./access";
import reducerLogin from "./rebusPhold";

const reducers = combineReducers({
  accessStore,
  reducerLogin
});

const persistConfig = {
  key: "redux-sagas",
  storage
};

const rootReducer = (state, action) => {
  if (action.type === "USER_LOGOUT") {
    state = {};
    return reducers(state, action);
  }
  return reducers(state, action);
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const sagaMiddleware = createSagaMiddleware();
export default function configureStore() {
  let store = createStore(
    persistedReducer,
    compose(applyMiddleware(sagaMiddleware))
  );
  let persistor = persistStore(store);
  return { store, persistor, sagaMiddleware };
}
