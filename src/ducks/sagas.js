import { all } from "redux-saga/effects";
import { search } from "./access/sagas";
import { sagaPlaceH } from "./rebusPhold/sagas";

export default function* rootSaga() {
  yield all([search(), sagaPlaceH()]);
}
