import * as types from "./types";

const searchMovie = payload => {
  return {
    type: types.SEARCH_MOVIE_START,
    payload
  };
};

const searchMovieById = payload => {
  return {
    type: types.SEARCH_MOVIE_BY_ID_START,
    payload
  };
};

const addText = payload => {
  return {
    type: types.ADD_TEXT,
    payload
  };
};

const cutText = payload => {
  return {
    type: types.CUT_TEXT,
    payload
  };
};

export { searchMovie, searchMovieById, addText, cutText };
