import { put, call, takeLatest } from "redux-saga/effects";
import { get } from "lodash";
import * as types from "./types";
import { apiCall } from "../../api";

export function* searchMovie({ payload }) {
  try {
    const results = yield call(
      apiCall,
      `&s=${payload.movieName}`,
      null,
      null,
      "GET"
    );
    yield put({ type: types.SEARCH_MOVIE_COMPLETE, results });
  } catch (error) {
    yield put({ type: types.SEARCH_MOVIE_ERROR, error });
  }
}

export function* searchMovieById({ payload }) {
  try {
    const movie = yield call(
      apiCall,
      `&i=${payload.movieId}`,
      null,
      null,
      "GET"
    );
    yield put({ type: types.SEARCH_MOVIE_BY_ID_COMPLETE, movie }); // lanzar accion para que el reducer la detecte (payolad:results)
  } catch (error) {
    yield put({ type: types.SEARCH_MOVIE_BY_ID_ERROR, error }); // accion en caso de error (payolad:error)
  }
}

export function* search() {
  yield takeLatest(types.SEARCH_MOVIE_START, searchMovie);
  yield takeLatest(types.SEARCH_MOVIE_BY_ID_START, searchMovieById);
}

// PARA OBTENER DEL STATE REDUCER LAS VARIABLES DE ESTADO...:
export const isSearchLoading = state => get(state, "accessStore.isLoading");
export const movieResults = state =>
  get(state, "accessStore.movieResults.Search");
export const movieResult = state => get(state, "accessStore.movieResult");
export const saved = state => get(state, "accessStore.saved");
export const error = state => get(state, "accessStore.error");
