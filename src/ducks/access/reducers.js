import * as types from "./types";

let savedVar = [];

const InitialState = {};

// let saved = [];

export default function accessStore(state = InitialState, action) {
  switch (action.type) {
    case types.SEARCH_MOVIE_START:
      return {
        ...state,
        error: null,
        isLoading: true,
        movieResults: null
      };
    case types.SEARCH_MOVIE_COMPLETE:
      return {
        ...state,
        error: null,
        isLoading: false,
        movieResults: action.results.data
      };
    case types.SEARCH_MOVIE_ERROR:
      console.log("SEARCH_MOVIE_ERROR", action.error.message);
      return {
        ...state,
        error: action.error.message,
        isLoading: false,
        movieResults: null
      };

    case types.SEARCH_MOVIE_BY_ID_START: {
      return { ...state, error: null, isLoading: true, movieResult: null }; // movieResults: null PARA QUE NO SE VEA ENTRE LOADING ??????????
    }
    case types.SEARCH_MOVIE_BY_ID_ERROR: {
      console.log(action.error.message);
      return {
        ...state,
        error: action.error.message,
        isLoading: false,
        movieResult: null
      };
    }
    case types.SEARCH_MOVIE_BY_ID_COMPLETE: {
      // console.log(action);
      return {
        ...state,
        error: null,
        isLoading: false,
        movieResult: action.movie.data
      };
    }

    case types.ADD_TEXT: {
      // console.log("ADD_TEXT action: ", action);
      // return {...state, saved:[...state.saved, action.payload.searchText]};
      savedVar = [...savedVar, action.payload.searchText];
      return { ...state, saved: [...savedVar] };
    }
    case types.CUT_TEXT: {
      savedVar = state.saved.filter(
        (text, index) => index !== action.payload.index
      ); // Se guardan las filtradas en "let savedVar"
      return { ...state, saved: [...savedVar] };
    }

    default:
      return state;
  }
}
