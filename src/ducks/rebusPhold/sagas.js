import { put, call, takeLatest } from "redux-saga/effects";
import { get } from "lodash";
import * as types from "./types";
import { apiPlaceHolder } from "../../api";

export function* initialUsers({ payload }) {
  try {
    const iniUsers = yield call(apiPlaceHolder, `/posts`, null, null, "GET");
    yield put({ type: types.INITIAL_USERS_COMPLETE, iniUsers });
  } catch (error) {
    yield put({ type: types.INITIAL_USERS_ERROR, error });
  }
}

export function* show10({ payload }) {
  try {
    const users = yield call(apiPlaceHolder, `/posts`, null, null, "GET");
    yield put({ type: types.SHOW_10_COMPLETE, users });
  } catch (error) {
    yield put({ type: types.SHOW_10_ERROR, error });
  }
}

export function* postUser({ payload }) {
  try {
    const user = yield call(
      apiPlaceHolder,
      `/posts`,
      payload.user,
      null,
      "POST"
    );
    yield put({ type: types.POST_USER_COMPLETE, user });
  } catch (error) {
    yield put({ type: types.POST_USER_ERROR, error });
  }
}

export function* upDateUser({ payload }) {
  try {
    const userModif = yield call(
      apiPlaceHolder,
      `/posts/${payload.id}`,
      payload.user,
      null,
      "PUT"
    );
    yield put({ type: types.UP_DATE_USER_COMPLETE, userModif });
  } catch (error) {
    yield put({ type: types.UP_DATE_USER_ERROR, error });
  }
}

export function* upDatePart({ payload }) {
  try {
    const partModif = yield call(
      apiPlaceHolder,
      `/posts/${payload.id}`,
      payload.user,
      null,
      "PATCH"
    );
    yield put({ type: types.UP_DATE_PART_COMPLETE, partModif });
  } catch (error) {
    yield put({ type: types.UP_DATE_PART_ERROR, error });
  }
}

export function* deleteUser({ payload }) {
  try {
    const userDele = yield call(
      apiPlaceHolder,
      `/posts/${payload.id}`,
      null,
      null,
      "DELETE"
    );
    yield put({
      type: types.DELETE_USER_COMPLETE,
      dataDele: { payload, userDele }
    });
  } catch (error) {
    yield put({ type: types.DELETE_USER_ERROR, error });
  }
}

export function* detailPostById({ payload }) {
  try {
    const detailPost = yield call(
      apiPlaceHolder,
      `/posts/${payload.idPostDetail}`,
      null,
      null,
      "GET"
    );
    // yield put({ type: types.DETAIL_POST_BY_ID_COMPLETE, detailPost });
    const comments = yield call(
      apiPlaceHolder,
      `/comments?postId=${payload.idPostDetail}`,
      null,
      null,
      "GET"
    );
    yield put({
      type: types.DETAIL_POST_BY_ID_COMPLETE,
      dataDetail: { detailPost, comments }
    });
  } catch (error) {
    yield put({ type: types.DETAIL_POST_BY_ID_ERROR, error });
  }
}

export function* seeComments({ payload }) {
  try {
    const comments = yield call(
      apiPlaceHolder,
      `/comments?postId=${payload.postId}`,
      null,
      null,
      "GET"
    );
    yield put({ type: types.SEE_COMMENTS_COMPLETE, comments });
  } catch (error) {
    yield put({ type: types.SEE_COMMENTS_ERROR, error });
  }
}

export function* postsUserId({ payload }) {
  try {
    const posts = yield call(
      apiPlaceHolder,
      `/posts?userId=${payload.userId}`,
      null,
      null,
      "GET"
    );
    yield put({ type: types.POSTS_USER_ID_COMPLETE, posts });
  } catch (error) {
    yield put({ type: types.POSTS_USER_ID_ERROR, error });
  }
}

export function* sagaPlaceH() {
  yield takeLatest(types.INITIAL_USERS_START, initialUsers);
  yield takeLatest(types.SHOW_10_START, show10);
  yield takeLatest(types.POST_USER_START, postUser);
  yield takeLatest(types.UP_DATE_USER_START, upDateUser);
  yield takeLatest(types.UP_DATE_PART_START, upDatePart);
  yield takeLatest(types.DELETE_USER_START, deleteUser);
  yield takeLatest(types.DETAIL_POST_BY_ID_START, detailPostById);
  yield takeLatest(types.SEE_COMMENTS_START, seeComments);
  yield takeLatest(types.POSTS_USER_ID_START, postsUserId);
}

// PARA OBTENER DEL STATE REDUCER LAS VARIABLES DE ESTADO...:
export const isSearchLoading = state => get(state, "reducerLogin.isLoading");
export const users = state => get(state, "reducerLogin.users");
export const scroll10 = state => get(state, "reducerLogin.scroll10");
export const detailPost = state => get(state, "reducerLogin.detailPost");
export const comments = state => get(state, "reducerLogin.comments");
export const postsUserIdList = state => get(state, "reducerLogin.postsUserId");
export const error = state => get(state, "reducerLogin.error");
