import * as types from "./types";
import { initialUsers } from "./actions";

let usersVar = []; // para resguardar la varieble de estado state.users

const InitialState = {};

// let saved = [];

export default function reducerLogin(state = InitialState, action) {
  switch (action.type) {
    case types.INITIAL_USERS_START:
      return {
        ...state,
        error: null,
        isLoading: true,
        users: null
      };
    case types.INITIAL_USERS_COMPLETE:
      const initial10 = action.iniUsers.data.slice(0, 10); // extraccion de los (10) primeros elemens del array
      usersVar = initial10; // guardo los primeros 10
      return {
        ...state,
        isLoading: false,
        error: null,
        users: [...initial10]
      };
    case types.INITIAL_USERS_ERROR:
      console.log(action.error.request);
      // action.error.response.status
      return {
        ...state,
        error: action.error.message,
        isLoading: false,
        users: null
      };
    // SHOW_10
    case types.SHOW_10_START:
      return {
        ...state,
        scroll10: false,
        isLoading: true,
        error: null,
        users: null
      };
    case types.SHOW_10_COMPLETE:
      // const indexIni = state.users.length; // No, ya que el state.users viene como (null) por la action anterior START
      const indexIni = usersVar.length; // length de lo que llevaba el state.users anteriormente
      const indexLast = indexIni + 10;

      const more10 = action.users.data.slice(indexIni, indexLast); // extraccion de los (10) siguientes elemens del array
      usersVar = [...usersVar, ...more10]; // actualizo la variable de respaldo para agregarle los nuevos 10 extraidos
      return {
        ...state,
        scroll10: true,
        isLoading: false,
        error: null,
        users: [...usersVar] // solo le agrego usersVar porque ya le incluí los nuevos 10
      };
    case types.SHOW_10_ERROR:
      return {
        ...state,
        scroll10: false,
        isLoading: false,
        error: action.error.message,
        users: null
      };
    // POST_USER
    case types.POST_USER_START:
      return {
        ...state,
        isLoading: true,
        error: null,
        users: null
      };
    case types.POST_USER_COMPLETE:
      console.log("reducer_POST_USER:user", action.user);
      // usersVar = [action.user.data, ...usersVar]; // Agrego el post al principio del array de respaldo
      return {
        ...state,
        isLoading: false,
        error: null,
        users: [action.user.data, ...usersVar] // ...
      };
    case types.POST_USER_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.error.message,
        users: null
      };
    // UP_DATE_USER
    case types.UP_DATE_USER_START:
      return {
        ...state,
        isLoading: true,
        error: null,
        users: null
      };
    case types.UP_DATE_USER_COMPLETE:
      console.log("reducer_UP_DATE_USER:userModif", action.userModif);
      usersVar.splice(action.userModif.data.id - 1, 1, action.userModif.data);
      return {
        ...state,
        isLoading: false,
        error: null,
        users: [...usersVar] // ...
      };
    case types.UP_DATE_USER_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.error.message,
        users: null
      };
    // UP_DATE_PART
    case types.UP_DATE_PART_START:
      return {
        ...state,
        isLoading: true,
        error: null,
        users: null
      };
    case types.UP_DATE_PART_COMPLETE:
      console.log("reducer_UP_DATE_PART:partModif", action.partModif);
      usersVar.splice(action.partModif.data.id - 1, 1, action.partModif.data);
      return {
        ...state,
        isLoading: false,
        error: null,
        users: [...usersVar] // ...
      };
    case types.UP_DATE_PART_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.error.message,
        users: null
      };
    // DELETE_USER
    case types.DELETE_USER_START:
      return {
        ...state,
        error: null,
        isLoading: true,
        error: null,
        users: null
      };
    case types.DELETE_USER_COMPLETE:
      console.log("reducer_DELETE_USER:dataDele", action.dataDele);
      usersVar = usersVar.filter(
        (user, index) => user.id !== action.dataDele.payload.id // filtrar la variable de resguardo para luego desplegarla sin el user eliminado
      );
      return {
        ...state,
        error: null,
        isLoading: false,
        users: [...usersVar] // ...
      };
    case types.DELETE_USER_ERROR:
      console.log(action.error);
      return {
        ...state,
        isLoading: false,
        error: action.error.message,
        users: null
      };
    // DETAIL_POST_BY_ID
    case types.DETAIL_POST_BY_ID_START:
      return {
        ...state,
        isLoading: true,
        error: null,
        detailPost: null
      };
    case types.DETAIL_POST_BY_ID_COMPLETE:
      console.log(
        "reducer_DETAIL_POST_BY_ID:action.dataDetail.comments",
        action.dataDetail.detailPost.data
      );
      return {
        ...state,
        isLoading: false,
        error: null,
        detailPost: action.dataDetail.detailPost.data
      };
    case types.DETAIL_POST_BY_ID_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.error.message,
        detailPost: null
      };
    // SEE_COMMENTS
    case types.SEE_COMMENTS_START:
      return {
        ...state,
        isLoading: true,
        error: null,
        comments: null
      };
    case types.SEE_COMMENTS_COMPLETE:
      console.log("action_SEE_COMMENTS_reducer:", action.comments.data);
      return {
        ...state,
        isLoading: false,
        error: null,
        comments: action.comments.data
      };
    case types.SEE_COMMENTS_ERROR:
      console.log(action.error);
      return {
        ...state,
        isLoading: false,
        error: action.error.message,
        comments: null
      };
    // POSTS_USER_ID
    case types.POSTS_USER_ID_START:
      return {
        ...state,
        isLoading: true,
        error: null,
        postsUserId: null
      };
    case types.POSTS_USER_ID_COMPLETE:
      console.log("action_POSTS_USER_ID_reducer:", action.posts.data);
      return {
        ...state,
        isLoading: false,
        error: null,
        postsUserId: action.posts.data
      };
    case types.POSTS_USER_ID_ERROR:
      console.log(action.error);
      return {
        ...state,
        isLoading: false,
        error: action.error.message,
        postsUserId: null
      };

    default:
      return state;
  }
}
