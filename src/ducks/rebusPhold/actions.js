import * as types from "./types";

let saved;

const initialUsers = payload => {
  console.log("ACTION_initialUsers");

  return {
    type: types.INITIAL_USERS_START,
    payload
  };
};

const show10 = payload => {
  console.log("show_10_ACTION");

  return {
    type: types.SHOW_10_START,
    payload
  };
};

const postUser = payload => {
  console.log("post_User_ACTION");

  return {
    type: types.POST_USER_START,
    payload
  };
};

const upDateUser = payload => {
  console.log("UpDateUser_ACTION", payload);

  return {
    type: types.UP_DATE_USER_START,
    payload
  };
};

const upDatePart = payload => {
  console.log("UpDatePart_ACTION", payload);

  return {
    type: types.UP_DATE_PART_START,
    payload
  };
};

const deleteUser = payload => {
  console.log("deleteUser_ACTION", payload);

  return {
    type: types.DELETE_USER_START,
    payload
  };
};

const detailPostById = payload => {
  console.log("detailPostById_ACTION", payload);

  return {
    type: types.DETAIL_POST_BY_ID_START,
    payload
  };
};

const seeComments = payload => {
  console.log("seeComments_ACTION", payload);

  return {
    type: types.SEE_COMMENTS_START,
    payload
  };
};

const postsUserId = payload => {
  console.log("postsUserId_ACTION", payload);

  return {
    type: types.POSTS_USER_ID_START,
    payload
  };
};

export {
  initialUsers,
  show10,
  postUser,
  upDateUser,
  upDatePart,
  deleteUser,
  detailPostById,
  seeComments,
  postsUserId
};
